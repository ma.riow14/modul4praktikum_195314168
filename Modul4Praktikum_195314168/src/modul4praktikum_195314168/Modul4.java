package modul4praktikum_195314168;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class Modul4 extends JFrame implements ActionListener {

    private static final int FRAME_WIDTH = 500;
    private static final int FRAME_HEIGHT = 500;
    private static final int FRAME_X_ORIGIN = 150;
    private static final int FRAME_Y_ORIGIN = 250;
    private JLabel text1;
    private JLabel text2;
    private JLabel hasil;
    private JButton button_hasil;
    private JTextField bilangan1;
    private JTextField bilangan2;
    private JTextField hasil_bilangan;

    public static void main(String[] args) {
        Modul4 frame = new Modul4();
        frame.setVisible(true);
    }

    public Modul4() {
        Container contentPane = getContentPane();
        contentPane.setLayout(null);


        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        setResizable(false);
        setTitle("Input Data");
        setLocation(FRAME_X_ORIGIN, FRAME_Y_ORIGIN);

        text1 = new JLabel("Bilangan 1");
        text1.setBounds(0, 15, 80, 60);
        text1.setFont(new Font("Tahoma",1,14));
        contentPane.add(text1);

        text2 = new JLabel("Bilangan 2");
        text2.setBounds(0, 65, 80, 60);
        text2.setFont(new Font("Tahoma",1,14));
        contentPane.add(text2);

        hasil = new JLabel("Hasil");
        hasil.setBounds(0, 121, 60, 60);
        hasil.setFont(new Font("Tahoma",1,14));
        contentPane.add(hasil);

        bilangan1 = new JTextField();
        bilangan1.setBounds(120, 30, 300, 35);
        contentPane.add(bilangan1);

        bilangan2 = new JTextField();
        bilangan2.setBounds(120, 80, 300, 35);
        contentPane.add(bilangan2);

        hasil_bilangan = new JTextField();
        hasil_bilangan.setBounds(120, 135, 300, 35);
        contentPane.add(hasil_bilangan);

        button_hasil = new JButton("Jumlah");
        button_hasil.setBounds(105, 185, 100, 50);
        button_hasil.setFont(new Font("Tahoma",1,14));
        contentPane.add(button_hasil);

        button_hasil.addActionListener(this);

        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    @Override
    public void actionPerformed(ActionEvent ae) {

        int a, b;
        if (ae.getSource() == button_hasil) {
            a = Integer.parseInt(bilangan1.getText());
            b = Integer.parseInt(bilangan2.getText());

            hasil_bilangan.setText("" +(a + b));
        }
    }
}
